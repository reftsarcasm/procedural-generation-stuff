﻿using System.Windows;
using MidpointDisplacementApp.ViewModel;
using MidpointDsplacement.PointGen2d;

namespace MidpointDisplacementApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public IViewModel ViewModel { get; private set; }
        public MainWindow()
        {
            ViewModel = new PointViewModel()
            {
                PointGenerator = new MidpointDisplacementGenerator(),
                Height = 325,
                Width = 500
            };
            InitializeComponent();
        }

        private void RefreshPointsClick(object sender, RoutedEventArgs e)
        {
            ViewModel.RefreshLines();
        }
    }
}
