﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Shapes;
using MidpointDsplacement.PointGen2d;

namespace MidpointDisplacementApp.ViewModel
{
    public interface IViewModel : INotifyPropertyChanged
    {
        /// <summary>
        /// Lines to draw on the canvas
        /// </summary>
        ObservableCollection<Line> Lines { get; }
        /// <summary>
        /// Height of the canvas
        /// </summary>
        int Height { get; set; }
        /// <summary>
        /// Width of the canvas
        /// </summary>
        int Width { get; set; }
        /// <summary>
        /// Iterations in generation
        /// </summary>
        int Iterations { get; set; }
        /// <summary>
        /// Point generator
        /// </summary>
        IGenerator PointGenerator { get; set; }
        /// <summary>
        /// Roughness value for the generation
        /// </summary>
        double RoughnessValue { get; set; }
        /// <summary>
        /// Refresh the lines in the view model
        /// </summary>
        void RefreshLines();
    }
}
