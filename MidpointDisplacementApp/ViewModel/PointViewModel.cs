﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Media;
using System.Windows.Shapes;
using MidpointDisplacementApp.Annotations;
using MidpointDsplacement.PointGen2d;
using Point = System.Drawing.Point;

namespace MidpointDisplacementApp.ViewModel
{
    public class PointViewModel : IViewModel
    {
        private int _iterations;
        private ObservableCollection<Line> _lines;
        private int _height;
        private int _width;
        public event PropertyChangedEventHandler PropertyChanged;

        public PointViewModel()
        {
            _lines = new ObservableCollection<Line>();
            RoughnessValue = 1;
        }
        public ObservableCollection<Line> Lines
        {
            get { return _lines; }
        }

        public int Height
        {
            get { return _height; }
            set
            {
                _height = value;
                OnPropertyChanged("Height");
            }
        }

        public int Width
        {
            get { return _width; }
            set
            {
                _width = value;
                OnPropertyChanged("Width");
            }
        }

        public int Iterations
        {
            get { return _iterations; }
            set
            {
                _iterations = value;
                OnPropertyChanged("Iterations");
            }
        }

        public IGenerator PointGenerator { get; set; }
        public double RoughnessValue { get; set; }

        public void RefreshLines()
        {
            PointGenerator.RefreshPoints(Width, Height, Iterations, RoughnessValue);
            Lines.Clear();
            var points = new List<Point>(PointGenerator.Points);
            var count = points.Count - 1;
            for (var i = 0; i < count; i++)
            {
                Lines.Add(GetLine(points[i], points[i + 1]));
            }
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        protected Line GetLine(Point p1, Point p2)
        {
            return new Line()
            {
                X1 = p1.X,
                X2 = p2.X,
                Y1 = p1.Y,
                Y2 = p2.Y,
                StrokeThickness = 2,
                Stroke = new SolidColorBrush(Color.FromRgb(0,0,0))
            };
        }
    }
}
