﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;

namespace DiamondSquareApp.ViewModels
{
    public interface IDiamondSquareViewModel : INotifyPropertyChanged
    {
        ObservableCollection<Point3D> Points { get; }

        Point3D CameraPosition { get; set; }
        Point3D CameraLookDirection { get; set; }
    }
}
