﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;
using DiamondSquareApp.Annotations;

namespace DiamondSquareApp.ViewModels
{
    class DiamondSquareViewModel : IDiamondSquareViewModel
    {
        private ObservableCollection<Point3D> _points;
        private Point3D _cameraPosition;
        private Point3D _cameraLookDirection;
        public event PropertyChangedEventHandler PropertyChanged;

        public DiamondSquareViewModel()
        {
            Points = new ObservableCollection<Point3D>();
            CameraPosition = new Point3D(0,0,5);
            CameraLookDirection = new Point3D(0,0,-1);
        }

        public ObservableCollection<Point3D> Points
        {
            get { return _points; }
            private set
            {
                _points = value;
                OnPropertyChanged("Points");
            }
        }

        public Point3D CameraPosition
        {
            get { return _cameraPosition; }
            set
            {
                _cameraPosition = value;
                OnPropertyChanged("CameraPosition");
            }
        }

        public Point3D CameraLookDirection
        {
            get { return _cameraLookDirection; }
            set
            {
                _cameraLookDirection = value;
                OnPropertyChanged("CameraLookDirection");
            }
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
