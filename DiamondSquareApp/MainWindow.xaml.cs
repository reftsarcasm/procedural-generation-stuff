﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DiamondSquareApp.ViewModels;

namespace DiamondSquareApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public IDiamondSquareViewModel ViewModel { get; set; }
        public MainWindow()
        {
            ViewModel = new DiamondSquareViewModel();
            InitializeComponent();
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            var pos = ViewModel.CameraPosition;
            var key = e.Key;
            if (key == Key.A)
            {
                var newX = pos.X + 1;
                ViewModel.CameraPosition = new Point3D(newX, pos.Y, pos.Z);
            }
            else if (key == Key.D)
            {
                var newX = pos.X - 1;
                ViewModel.CameraPosition = new Point3D(newX, pos.Y, pos.Z);
            }
            else if (key == Key.W)
            {
                var newZ = pos.Z + 1;
                ViewModel.CameraPosition = new Point3D(pos.X, pos.Y, newZ);
            }
            else if (key == Key.S)
            {
                var newZ = pos.Z - 1;
                ViewModel.CameraPosition = new Point3D(pos.X, pos.Y, newZ);
            }
        }
    }
}
