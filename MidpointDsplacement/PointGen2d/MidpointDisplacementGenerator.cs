﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MidpointDsplacement.PointGen2d
{
    public class MidpointDisplacementGenerator : IGenerator
    {
        private List<Point> _points;
        private Random _random;
        public MidpointDisplacementGenerator()
        {
            _points = new List<Point>();
            _random = new Random();
        }

        public IEnumerable<Point> Points { get { return _points; } }

        /// <summary>
        /// Refresh the points
        /// </summary>
        /// <param name="width">Of the canvas</param>
        /// <param name="height">Height of the canvas</param>
        /// <param name="iterations">Iterations of the algorith</param>
        /// <param name="roughness"></param>
        /// <remarks></remarks>
        public void RefreshPoints(int width, int height, int iterations, double roughness)
        {
            _points.Clear();
            var midY = height/2;
            var p1 = new Point(0, midY);
            var p2 = new Point(width, midY);
            _points.Add(p1);
            _points.Add(p2);
            var maxDisplacement = height / 2d;
            for (var i = 1; i <= iterations; i++)
            {
                RunIteration(maxDisplacement);
                maxDisplacement *= roughness;
            }
        }

        private void RunIteration(double maxDisplacement)
        {
            var count = _points.Count - 1;
            var pointsToAdd = new List<Point>();
            for (var i = 0; i < count; i++)
            {
                var p1 = _points[i];
                var p2 = _points[i + 1];
                var midX = (p1.X + p2.X)/2;
                var midY = (p1.Y + p2.Y) / 2;
                var sign = _random.Next(2) == 0 ? -1 : 1;
                var displacement = Convert.ToInt32(Math.Round(sign * _random.NextDouble() * maxDisplacement));
                pointsToAdd.Add(new Point(midX, midY + displacement));
            }
            _points.AddRange(pointsToAdd);
            _points.Sort((p1, p2) => p1.X - p2.X);
        }
    }
}
